export const loginForm = [
  {label: 'Ingrese su nombre de usuario', name: 'username', type: 'text'},
  {label: 'Y su contraseña', name: 'password', type: 'password'}
]

export const registroForm = [
  {label: 'Defina su nombre de usuario', name: 'username', type: 'text'},
  {label: 'Ingrese su contraseña', name: 'password', type: 'password'},
  {label: 'Repitala', name: 'repeat_password', type: 'password'},
  {label: 'Que cuenta desea?', name: 'tipodeusuario', type: ''},
]

export const nuevaAppForm = [
  {label: 'Nombre', name: 'nombre', type: 'text'},
  {label: 'Categoria', name: 'categoriaId', type: 'number'},
  {label: 'Url de la imagen', name:'imageurl', type: 'text'},
  {label: 'Precio', name: 'precio', type: 'number'}
]

export const editarAppForm = [
  {label: 'Url de la imagen', name:'imageurl', type: 'text'},
  {label: 'Precio', name: 'precio', type: 'number'}
]