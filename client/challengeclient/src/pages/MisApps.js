import React, { Component } from 'react'
import { connect } from 'react-redux';
import * as actions from '../actions/';
import Identification from '../components/Identification'
import ClientStore from '../components/ClientStore'
import App from '../components/appsubcomponents/app'
import Appcomprada from '../components/appsubcomponents/appcomprada'
import Appguest from '../components/appsubcomponents/appguest'
import Appdev from '../components/appsubcomponents/appeditcontainer'
import _ from 'lodash'
class MisApps extends Component {
  componentDidMount() {
    console.log(this.props)
    const {history, getAppsGuest, getAppsClient} = this.props
    // fetchCategorias()
    
    const {isLoggedIn, tipodeusuario} = this.props.auth
    const token = this.props.auth.token
    if (!isLoggedIn) {
      getAppsGuest()
    } else {
      if (tipodeusuario == 'cliente') {
        getAppsClient({tipo:'misapps'}, token)
      } else {
        //No deberia llamarse cliente, pero bueno xD
        getAppsClient({}, token)
      }
    }
  }
  renderCategories() {
    const {categorias} = this.props
    return _.map(categorias, cat => {
      return <div className="col-sm-2">
       <button className="btn" onClick={() => this.getAppsPorCategoria(cat.id) }> {cat.nombre} </button> </div>
    })
  }
  getAppsPorCategoria(id) {
    const {getAppsGuest} = this.props
    const token = this.props.auth.token
    getAppsGuest({categoria: id}, token)
  }
  renderClientList () {
    const {apps} = this.props
    return _.map(apps.resultados, application => {
      return <div className="col-sm-4">
        <Appcomprada app={application} />
        </div>
    })
  }
  renderGuestList () {
    const {apps, history} = this.props
    return _.map(apps.resultados, application => {
      return <div className="col-sm-4">
        <Appguest app={application} history={history}/>
        </div>
    })
  }
  renderDevList () {
    const {apps} = this.props
    return _.map(apps.resultados, application => {
      return <div className="col-sm-4">
        <Appdev app={application} editar={this.UpdateApp} />
        </div>
    })
  }
  UpdateApp = (props) => {
    const {editarApp, auth} = this.props
    editarApp(props, auth.token)
  }
  renderList () {
    const {tipodeusuario} = this.props.auth
    switch (tipodeusuario) {
      case 'cliente': return <div className="row">{this.renderClientList()} </div>
      case 'desarrollador': return <div className="row">{this.renderDevList()} </div>
      default: return  <div> <div className="row">{this.renderCategories()} </div>
      <hr/>
      <div className="row">
      {this.renderGuestList()}
      </div>
      </div>
    }
  }
  render() {
    return <div className="container"> 
    {this.renderList()}
    </div>
  }
}
const mapStateToProps = (state) => {
  return { auth: state.auth, apps: state.apps, categorias: state.categorias, miscompras: state.miscompras, misdeseos: state.misdeseos}
}
export default connect(mapStateToProps, actions)(MisApps)