import React, { Component } from 'react'
import { connect } from 'react-redux';
import * as actions from '../actions/';
import Identification from '../components/Identification'
import ClientStore from '../components/ClientStore'
import App from '../components/appsubcomponents/app'
import _ from 'lodash'
class MainPage extends Component {
  componentDidMount() {
    console.log(this.props)
    const {getAppsClient, history} = this.props
    const {isLoggedIn, tipodeusuario, token} = this.props.auth
    if (isLoggedIn) {
      getAppsClient({tipo:'misappsdeseadas'}, token)
    } else {
      history.push('/')
    }
    // fetchCategorias()
  }
  comprarApp = (id) => {
    const token = this.props.auth.token
    const {comprarApp} = this.props
    comprarApp(id, token)
  }
  desearApp = (id, esDeseada) => {
    const token = this.props.auth.token
    const {desearApp, unDesearApp} = this.props
    if (esDeseada) {
      unDesearApp(id, token)
    } else {
      desearApp(id, token)
    }
  }
  renderList () {
    const {apps, misdeseos} = this.props
    return _.map(apps.resultados, application => {
      if (misdeseos.resultados.find(o => o.appId == application.id)) {
        return <div className="col-sm-4">
        <App app={application} comprar={this.comprarApp} desear={this.desearApp} deseada={true} />
        </div>
      } else {
        return <div />
      }
      
    })
  }
  render() {
    const { history} = this.props
    const {isLoggedIn, tipodeusuario} = this.props.auth
    if (!isLoggedIn && tipodeusuario != 'cliente') {
      history.push('/')
    }
    return <div className="container">
    <div className="row">{this.renderList()}</div>
    </div>
  }
}
const mapStateToProps = (state) => {
  return { auth: state.auth, apps: state.apps, categorias: state.categorias, miscompras: state.miscompras, misdeseos: state.misdeseos}
}
export default connect(mapStateToProps, actions)(MainPage)