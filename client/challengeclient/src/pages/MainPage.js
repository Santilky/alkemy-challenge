import React, { Component } from 'react'
import { connect } from 'react-redux';
import * as actions from '../actions/';
import Identification from '../components/Identification'
import ClientStore from '../components/ClientStore'
class MainPage extends Component {
  componentDidMount() {
    console.log(this.props)
    const {fetchCategorias} = this.props
    // fetchCategorias()
  }
  render() {
    const isLoggedIn = this.props.auth.isLoggedIn
    const {history} = this.props
    // console.log(this.props.auth)
    // const isLoggedIn = false
    // console.log(this.state.auth)
    return <div>{isLoggedIn ? <ClientStore history={history} /> : <Identification />} </div>
  }
}
const mapStateToProps = (state) => {
  return { auth: state.auth}
}
export default connect(mapStateToProps, actions)(MainPage)