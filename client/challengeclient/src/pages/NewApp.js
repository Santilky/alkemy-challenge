import React, { Component } from 'react'
import { connect } from 'react-redux';
import * as actions from '../actions/';
import NewApp from '../components/newapp'
class MainPage extends Component {
  componentDidMount() {
    // console.log(this.props)
    const {fetchCategorias, history} = this.props
    // fetchCategorias()
  }
  render() {
    const {auth, history} = this.props
    if (!auth.isLoggedIn || auth.tipodeusuario == 'cliente') {
      history.push('/')
    }
    // console.log(this.props.auth)
    // const isLoggedIn = false
    // console.log(this.state.auth)
    return <div className="d-flex justify-content-center align-self-center">
      <NewApp history={history} />
    </div>
  }
}
const mapStateToProps = (state) => {
  return { auth: state.auth}
}
export default connect(mapStateToProps, actions)(MainPage)