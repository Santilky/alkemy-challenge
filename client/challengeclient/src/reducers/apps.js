import {GET_APPS, LOGOUT} from '../actions/types'

export default function(state = {totaldeapps: 0, resultados: []}, action) {
  
  switch (action.type) {
    case GET_APPS: 
      return action.payload
      case LOGOUT: 
      return {totaldeapps: 0, resultados: []}
    default:
      return state
  }
}