import {GET_MISCOMPRAS} from '../actions/types'

export default function(state = {totaldeapps: 0, resultados: []}, action) {
  switch (action.type) {
    case GET_MISCOMPRAS: 
      return action.payload
    default:
      return state
  }
}