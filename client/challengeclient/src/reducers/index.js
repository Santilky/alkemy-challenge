import {combineReducers} from 'redux'
import { reducer as reduxForm } from 'redux-form';
import session from './session'
import categorias from './categorias'
import apps from './apps'
import miscompras from './compras'
import misdeseos from './deseos'

export default combineReducers({
  form: reduxForm,
  registerForm: reduxForm,
  auth: session,
  categorias: categorias,
  apps,
  miscompras,
  misdeseos,
  newAppForm: reduxForm,
  editAppForm: reduxForm,
})