import {LOGIN, LOGOUT} from '../actions/types'

export default function(state = {isLoggedIn: false, token: null, tipodeusuario: null}, action) {
  switch (action.type) {
    case LOGIN: 
      return {isLoggedIn: true, token: 'bearer ' + action.payload.token, tipodeusuario: action.payload.tipodeusuario}
    case LOGOUT: return {isLoggedIn: false, token: null, tipodeusuario: null}
    default:
      return state
  }
}