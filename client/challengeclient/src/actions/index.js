import Axios from 'axios'
import {REGISTER, LOGIN, GET_CATEGORIAS, GET_APPS, GET_MISCOMPRAS, GET_MISDESEOS, LOGOUT} from './types'

const axios = Axios.create({
  baseURL: 'http://localhost:3500/api'
})
export const fetchRegister = (props) => async dispatch => {
  const user = await axios.post('/user', {
    username: props.username,
    password: props.password,
    repeat_password: props.password,
    tipodeusuario: props.tipodeusuario
  })
  dispatch({type: REGISTER, payload: user.data})
}

export const fetchLogin = (props) => async dispatch => {
  const session = await axios.post('/session/login', {
    username: props.username,
    password: props.password
  })
  dispatch({type: LOGIN, payload: session.data})
}
export const logout = () => async dispatch => {
  dispatch({type: LOGOUT, payload: {}})
}
export const fetchCategorias = (token) => async dispatch => {
  const headers = {
    'Content-Type': 'application/json',
    "Authorization": token
  }
  const categorias = await Axios.get('http://localhost:3500/api/categorias', {headers})
  dispatch({type: GET_CATEGORIAS, payload: categorias.data})
}

export const getAppsClient = (params, token) => async dispatch => {
  const headers = {
    'Content-Type': 'application/json',
    "Authorization": token
  }
  const apps = await Axios.get('http://localhost:3500/api/apps/', {headers, params})
  ordenarResultadosAlfabeticamente(apps.data.resultados)
  dispatch({type: GET_APPS, payload: apps.data})
}
export const getAppsGuest = (params, token) => async dispatch => {
  const headers = {
    'Content-Type': 'application/json',
    "Authorization": token
  }
  const apps = await Axios.get('http://localhost:3500/api/apps/guest', {headers, params})
  ordenarResultadosAlfabeticamente(apps.data.resultados)
  dispatch({type: GET_APPS, payload: apps.data})
}
export const getDeseoClient = (token) => async dispatch => {
  const headers = {
    'Content-Type': 'application/json',
    "Authorization": token
  }
  const desearApp = await Axios.get('http://localhost:3500/api/apps/deseo', {headers})
  dispatch({type: GET_MISDESEOS, payload: desearApp.data})
}

export const getAppsCompradas = (token) => async dispatch => {
  const headers = {
    'Content-Type': 'application/json',
    "Authorization": token
  }
  const desearApp = await Axios.get('http://localhost:3500/api/apps/compras', {headers})
  dispatch({type: GET_MISCOMPRAS, payload: desearApp.data})

}

export const desearApp = (id, token) => async dispatch =>  {
  const headers = {
    'Content-Type': 'application/json',
    "Authorization": token
  }
  await Axios.post('http://localhost:3500/api/apps/deseo', {appid: id}, {headers})
  const desearApp = await Axios.get('http://localhost:3500/api/apps/deseo', {headers})
  dispatch({type: GET_MISDESEOS, payload: desearApp.data})
}

export const unDesearApp = (id, token) => async dispatch => {
  const headers = {
    'Content-Type': 'application/json',
    "Authorization": token
  }
  await Axios.delete('http://localhost:3500/api/apps/deseo/' + id, {headers})
  const desearApp = await Axios.get('http://localhost:3500/api/apps/deseo', {headers})
  dispatch({type: GET_MISDESEOS, payload: desearApp.data})
}

export const comprarApp = (id, token) => async dispatch =>  { 
  const headers = {
    'Content-Type': 'application/json',
    "Authorization": token
  }
  await Axios.post('http://localhost:3500/api/apps/compras', {appid: id}, {headers})
  const desearApp = await Axios.get('http://localhost:3500/api/apps/deseo', {headers})
  dispatch({type: GET_MISDESEOS, payload: desearApp.data})
  const appComprada = await Axios.get('http://localhost:3500/api/apps/compras', {headers})
  dispatch({type: GET_MISCOMPRAS, payload: appComprada.data})
}

export const crearApp = (app, token, history) => async dispatch => {
  const headers = {
    'Content-Type': 'application/json',
    "Authorization": token
  }
  await Axios.post('http://localhost:3500/api/apps', app, {headers})
  const apps = await Axios.get('http://localhost:3500/api/apps/', {headers})
  ordenarResultadosAlfabeticamente(apps.data.resultados)
  dispatch({type: GET_APPS, payload: apps.data})
  history.push('/apps')
}

export const editarApp = (props, token)  => async dispatch => {
  const headers = {
    'Content-Type': 'application/json',
    "Authorization": token
  }
   await Axios.put('http://localhost:3500/api/apps', props, {headers})
   const apps = await Axios.get('http://localhost:3500/api/apps/', {headers})
   ordenarResultadosAlfabeticamente(apps.data.resultados)
   dispatch({type: GET_APPS, payload: apps.data})
}
function ordenarResultadosAlfabeticamente(lista) {  
  console.log(lista)
  lista.sort((a, b) => a.nombre.localeCompare(b.nombre))
}

