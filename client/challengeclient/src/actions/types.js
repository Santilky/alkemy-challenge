export const REGISTER = 'fetch_register'
export const LOGIN = 'fetch_login'
export const LOGOUT = 'logout'
export const GET_CATEGORIAS = 'fetch_categorias'
export const GET_APPS = 'fetch_apps'
export const GET_MISCOMPRAS = 'fetch_mis_compras'
export const GET_MISDESEOS = 'fetch_mis_deseos'

