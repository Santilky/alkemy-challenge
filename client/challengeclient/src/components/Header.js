import React, {Component} from 'react'
import {connect} from 'react-redux'
import { Link } from 'react-router-dom';
import * as actions from '../actions/'

class Header extends Component {
  renderRightContent() {
    const {tipodeusuario} = this.props.auth
    // console.log('Tipo de usuario')
    // console.log(tipodeusuario)
    switch(tipodeusuario) {
      case 'cliente': return <div>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            {/* <Link className="nav-link" to="/">Log Out</Link>  */}
            <button className="btn btn-dark" onClick={() => this.logout()}> Log Out </button>
            </ul>
          </div>
      </div>

      case 'desarrollador': return   <div className="collapse navbar-collapse" id="navbarSupportedContent">
      <ul className="navbar-nav mr-auto">
        {/* <Link className="nav-link" to="/">Log Out</Link>  */}
        <button className="btn btn-dark" onClick={() => this.logout()}> Log Out </button>
        </ul>
      </div>
      default: return <div> </div>
    }
  }
  renderLeftContent() {
    const {tipodeusuario} = this.props.auth
    switch(tipodeusuario) {
      case 'cliente': return <ul className="navbar-nav mr-auto">
        <li className="nav-item active">
          <Link className="nav-link" to="/">Home <span className="sr-only">(current)</span></Link>
        </li>
      <li className="nav-item">
              <Link className="nav-link" to="/apps">Mis Apps</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/me/appsdeseadas">Lista de deseos</Link>
            </li>
            </ul>
      case 'desarrollador': return <ul className="navbar-nav mr-auto">
      <li className="nav-item active">
        <Link className="nav-link" to="/">Home <span className="sr-only">(current)</span></Link>
      </li>
      <li className="nav-item">
              <Link className="nav-link" to="/apps">Mis Apps</Link>
            </li>
            <li className="nav-item">
      <Link className="nav-link" to="/me/crearapp">Crear App</Link>
    </li>
      </ul>
    
      default: return <ul className="navbar-nav mr-auto">
      <li className="nav-item active">
        <Link className="nav-link" to="/apps"> Visita como invitado <span className="sr-only">(current)</span></Link>
      </li>
      </ul>
  }
}
  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <Link className="navbar-brand" to="/">Choogle Apps</Link>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
            {this.renderLeftContent()}
        </div>
        <div> 
          {this.renderRightContent()}
        </div>
      </nav>
    )
  }
  logout() {
    const {logout} = this.props
    logout()
  }
}

const mapStateToProps = (state) => {
  return { auth: state.auth}
}

export default connect(mapStateToProps, actions)(Header)