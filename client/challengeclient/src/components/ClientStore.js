import React, { Component } from 'react'
import * as actions from '../actions'
import { connect } from 'react-redux';
import _ from 'lodash'
import App from './appsubcomponents/app'
class ClientStore extends Component {
  componentDidMount() {
    
    const {getAppsClient, fetchCategorias, getAppsCompradas, getDeseoClient, history, auth} = this.props
    if (auth.tipodeusuario == 'desarrollador') {
      history.push('/apps') 
      return null
    }
    const token = this.props.auth.token
    getAppsClient({}, token)
    fetchCategorias(token)
    getDeseoClient(token)
    getAppsCompradas(token)
  }
  renderList () {
    const {apps, miscompras, misdeseos} = this.props
    return _.map(apps.resultados, application => {
      let deseada = false
      if (miscompras.resultados.find(o => o.appId == application.id)) {
        return <div />
      } else {
        if (misdeseos.resultados.find(o => o.appId == application.id)) {
          deseada = true
        }
      }
      return <div className="col-sm-4">
        <App app={application} comprar={this.comprarApp} desear={this.desearApp} deseada={deseada} />
        </div>
    })
  }
  comprarApp = (id) => {
    const token = this.props.auth.token
    const {comprarApp} = this.props
    comprarApp(id, token)
  }
  desearApp = (id, esDeseada) => {
    const token = this.props.auth.token
    const {desearApp, unDesearApp} = this.props
    if (esDeseada) {
      unDesearApp(id, token)
    } else {
      desearApp(id, token)
    }
  }
  renderCategories() {
    const {categorias} = this.props
    return _.map(categorias, cat => {
      return <div className="col-sm-2">
       <button className="btn" onClick={() => this.getAppsPorCategoria(cat.id) }> {cat.nombre} </button> </div>
    })
  }
  getAppsPorCategoria(id) {
    const {getAppsClient} = this.props
    const token = this.props.auth.token
    getAppsClient({categoria: id}, token)
  }
  getApps() {
    const {getAppsClient} = this.props
    const token = this.props.auth.token
    getAppsClient({}, token)
  }
  render() {
    const {apps, categorias} = this.props 
    console.log(apps.totaldeapps)
    console.log(apps.resultados)
    console.log(categorias)
    return <div className="container"> 
      <div className="row"> <div className="col-sm-2">Categorias:</div> 
      <div className="col-sm-2">
       <button className="btn" onClick={() => this.getApps() }> {'Todas'} </button> </div>
      {this.renderCategories()} </div>
      <hr/>
      <div className="row">
      {this.renderList()}
      </div>
    </div>
  }
}
const mapStateToProps = (state) => {
  return { auth: state.auth, apps: state.apps, categorias: state.categorias, miscompras: state.miscompras, misdeseos: state.misdeseos}
}
export default connect(mapStateToProps, actions)(ClientStore)