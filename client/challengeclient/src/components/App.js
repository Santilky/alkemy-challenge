import React, { Component } from 'react'
import {BrowserRouter, Route} from 'react-router-dom'
import Header from './Header'
//import Identification from './Identification'
import { connect } from 'react-redux';
import * as actions from '../actions';
import MainPage from '../pages/MainPage'
import Misapps from '../pages/MisApps'
import AppDeseadas from '../pages/DeseosApps'
import CrearApp from '../pages/NewApp'
class App extends Component {
  
  render() {
   return <div>
      <BrowserRouter>
        <Header />
        <Route path="/" exact component={MainPage}/>
        <Route path="/me/appsdeseadas" component={AppDeseadas}/>
        <Route path="/apps" component={Misapps}/>
        <Route path="/me/crearapp" component={CrearApp}/>
        <Route path="/me/editarapp" component={CrearApp}/>
      </BrowserRouter>
    </div>
  }
}

export default connect(null, actions)(App)