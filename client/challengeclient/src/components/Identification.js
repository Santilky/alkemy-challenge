import React, { Component } from 'react'
import Login from './Login'
import Register from './Register'
import { connect } from 'react-redux';
import * as actions from '../actions';

class Identification extends Component {
  constructor (props) {
    super(props)
    this.state = { loginPage: true}
  }
  
  renderContent() {
    if (this.state.loginPage) {
      return <Login onSubmit={this.onLogin} />
    } else {
      return <Register onSubmit={this.onRegister} />
    }
  }
  render() {
    console.log(this.props)
    return <div className="d-flex justify-content-center align-self-center">
    <div className="col-sm-4" style={styles.margintop}>
    <h2> Bienvenido a Choggle Apps</h2>
    <br/>
    <p>Necesitas logearte para acceder al sitio</p>
    <hr/>
      {this.renderContent()}
      
      <hr />
      <button className="btn d-flex justify-self-center" onClick={this.swapLogin}>{this.state.loginPage ? "Registrate" : "Logear"}</button>
    </div>
    </div>
  }
  onLogin = (values) => {
    const { fetchLogin } = this.props;

    // console.log(values)
    fetchLogin(values)
  }
  onRegister = (values) => {
    // console.log(values)
    const { fetchRegister } = this.props;
    fetchRegister(values)
    this.swapLogin()
  }
  swapLogin = () => {
    this.setState( {loginPage: !this.state.loginPage})
  }
}
const styles = {
  margintop: {
    marginTop: '10%',
  } 
}
const mapStateToProps = (state) => {
  return { auth: state.auth}
}

export default connect(mapStateToProps, actions)(Identification)