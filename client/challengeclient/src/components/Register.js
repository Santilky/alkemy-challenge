import React, { Component } from 'react'
import _ from 'lodash'
import {registroForm} from '../forms/formFields'
import {Field, reduxForm} from 'redux-form'
import FormField from './appsubcomponents/FormField'
class Register extends Component {
  renderFields() {
    return _.map(registroForm, ({label, name, type}) => {
      if (name != 'tipodeusuario') {
        return (
          <div>
          <Field
          key={name}
          component={FormField}
          label={label}
          name={name}
          type={type} />
          <br/>
          </div>
        )
      } else {
        return <div>
          {label}
         <Field key={name} name={name} label={label} className="form-control" component="select">
           <option />
        <option value="cliente">Cliente</option>
        <option value="desarrollador">Desarrollador</option>
      </Field>
      </div>
      }
      
    })
  }
  onSubmit(event) {
    event.preventDefault();
    //const form = document.getElementById('loginForm');
  //  console.log(form)

    //const { submitBlog, history, formValues } = this.props;
    //console.log(event)
    //submitBlog(formValues, history);
  }
  render() {
    const { pristine, submitting } = this.props;
    return (
      <div className="container">
        <form onSubmit={this.props.handleSubmit}>
          {this.renderFields()}
          <button type="submit" className="btn btn-dark">
            Registrar
          </button>
        </form>
      </div>
    )
  }
}


function validate(values) {
  const errors = {};

  _.each(registroForm, ({ name }) => {
    if (!values[name]) {
      errors[name] = 'El valor no es valido';
    }
  });

  return errors;
}
export default reduxForm({
  validate,
  form: 'registerForm',
  destroyOnUnmount: true
})(Register)