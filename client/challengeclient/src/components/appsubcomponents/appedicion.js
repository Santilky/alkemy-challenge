import React, { Component } from 'react'
import InputChange from './inputchange'

class Application extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  getUrl = (value) => {
    const newState = {...this.state, ['imageurl']: value};
    this.setState(newState)
  }
  getPrecio = (value) => {
      const newState = {...this.state, ['precio']: value};
      this.setState(newState)
  }
  render() {
    const {app, editar} = this.props
    return <div className="card" style={styles.card}>
    <img src={app.imageurl} className="card-img-top sm-col-3" alt="..." style={styles.img} />
    <div className="card-body">
      <h5 className="card-title">{app.nombre}</h5>
      <div className="card-text">
        <InputChange getValue={this.getUrl} type={'text'} texto={'Cambiar imagen'}/>
        <InputChange getValue={this.getPrecio} type={'number'} texto={'Cambiar precio'}/>
      </div>
      <div className="row">
        <button className="btn btn-warning" onClick={() => editar(this.state)} style={styles.button}>Terminar Edicion</button>
        </div>
    </div>
  </div>
  }
}
const styles = {
  button: {
    marginLeft: '5px',
    marginRight: '5px'
  },
  img: {
    padding: '2px'
  },
  card: {
    marginBottom: '20px'
  }
}
export default Application