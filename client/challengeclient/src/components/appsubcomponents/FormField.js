import React from 'react'

export default ({ input, label, type, meta: { error, touched } }) => {
  return (
    <div className={input.name}>
      <label>{label}</label>
      <input {...input} type={type} className="form-control" />
      <div className="danger"> 
        {touched && error}
      </div>
    </div>
  )
  
}