import React, { Component } from 'react'


class Application extends Component {

  render() {
    const {app, desear, comprar, deseada} = this.props
    return <div className="card" style={styles.card}>
    <img src={app.imageurl} className="card-img-top sm-col-3" alt="..." style={styles.img} />
    <div className="card-body">
      <h5 className="card-title">{app.nombre}</h5>
      <p className="card-text"></p>
      <div className="row">
        <button className="btn btn-warning" onClick={() => desear(app.id, deseada)} style={styles.button}> {deseada ? 'No lo deseo mas' : 'Desearlo!'}</button>
        <button className="btn btn-success" onClick={() => comprar(app.id)} style={styles.button}>{`Compra! $${app.precio}`} </button>
        </div>
    </div>
  </div>
  }
}
const styles = {
  button: {
    marginLeft: '5px',
    marginRight: '5px'
  },
  img: {
    padding: '2px'
  },
  card: {
    marginBottom: '20px'
  }
}
export default Application