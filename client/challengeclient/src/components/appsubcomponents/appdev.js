import React, { Component } from 'react'


class Application extends Component {

  render() {
    const {app, editar} = this.props
    return <div className="card" style={styles.card}>
    <img src={app.imageurl} className="card-img-top sm-col-3" alt="..." style={styles.img} />
    <div className="card-body">
      <h5 className="card-title">{app.nombre}</h5>
      <p className="card-text"></p>
      <div className="row">
        <button className="btn btn-warning" onClick={() => editar(app.id)} style={styles.button}>Editar</button>
        </div>
    </div>
  </div>
  }
}
const styles = {
  button: {
    marginLeft: '5px',
    marginRight: '5px'
  },
  img: {
    padding: '2px'
  },
  card: {
    marginBottom: '20px'
  }
}
export default Application