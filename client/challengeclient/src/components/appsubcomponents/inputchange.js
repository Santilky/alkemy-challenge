import React, { Component } from 'react'

class InputChange extends Component {
  constructor(props) {
    super(props)
    this.state = {value: ''}
  }

  render () {
     const {type, getValue, texto} = this.props
    return <div>
      <h5>{texto}</h5>
    <input type={type} className="form-control" onChange={(e) => 
    {this.setState({value: e.target.value})
      getValue(this.state)
    }} />
    <button className="btn" onClick={() => getValue(this.state.value)}> Confirmar</button>
    </div>
  }
}

export default InputChange