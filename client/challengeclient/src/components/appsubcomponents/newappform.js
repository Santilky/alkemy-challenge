import React, { Component } from 'react'
import {nuevaAppForm} from '../../forms/formFields'
import {Field, reduxForm} from 'redux-form'
import FormField from './FormField'
import _ from 'lodash'

class NewAppForm extends Component {
  renderFields() {
    const {categorias} = this.props
    return _.map(nuevaAppForm, ({label, name, type}) => {
      if (name == 'categoriaId'){
        return <div>
        {label}
       <Field key={name} name={name} label={label} className="form-control" component="select">
         <option />
         {_.map(categorias, ({nombre, id}) => {
           return <option value={id}>{nombre}</option>
         })}
    </Field>
    </div>
      }
      return (
        <div style={styles.marginVertical}>
        <Field
        key={name}
        component={FormField}
        label={label}
        name={name}
        type={type} />
        <br/>
        </div>
      )
    })
  }
  
  render () {
    return <div className="container">
      <h1>Crear nueva App</h1>
      <form onSubmit={this.props.handleSubmit}>
        {this.renderFields()}
        <button className="btn"> Crear!</button>
      </form>
    </div>
  }
}
function validate(values) {
  const errors = {};
  _.each(nuevaAppForm, ({ name }) => {
    if (!values[name]) {
      errors[name] = 'El valor no es valido';
    }
  });
  return errors;
}
const styles = {
  marginTop: '200px'
}
export default reduxForm({
  validate,
  form: 'newAppForm',
  destroyOnUnmount: true
})(NewAppForm)