import React, { Component } from 'react'
import App from './appdev'
import AppEdicion from './appedicion'

class EditAppContainer extends Component {
  state = {edit: false}

  edit = () => {
    this.setState({edit: true})
  }
  update = (props) => {
    const {editar, app} = this.props
    console.log(props)
    editar({
      id: app.id,
      cambios: props
    })
    this.setState({edit: false})
  }
  render() {
    console.log(this.state)
    const {app} = this.props
    if (!this.state.edit) {
      return <App app={app} editar={this.edit} />
    } else {
      return <AppEdicion app={app} editar={this.update} />
    }
  }

}
 export default EditAppContainer