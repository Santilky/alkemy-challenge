import React, { Component } from 'react'
import { Link } from 'react-router-dom'


class ApplicationGuest extends Component {

  render() {
    const {app, history} = this.props
    return <div className="card" style={styles.card}>
    <img src={app.imageurl} className="card-img-top sm-col-3" alt="..." style={styles.img} />
    <div className="card-body">
      <h5 className="card-title">{app.nombre}</h5>
      <p className="card-text"></p>
      <div className="row">
        <Link className="btn btn-primary" to="/">Logeate para comprarla</Link>
        </div>
    </div>
  </div>
  }
}
const styles = {
  button: {
    marginLeft: '5px',
    marginRight: '5px'
  },
  img: {
    padding: '2px'
  },
  card: {
    marginBottom: '20px'
  }
}
export default ApplicationGuest