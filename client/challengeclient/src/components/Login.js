import React, { Component } from 'react'
import _ from 'lodash'
import {loginForm} from '../forms/formFields'
import {Field, reduxForm} from 'redux-form'
import FormField from './appsubcomponents/FormField'
class Login extends Component {
  renderFields() {
    return _.map(loginForm, ({label, name, type}) => {
      return (
        <div style={styles.marginVertical}>
        <Field
        key={name}
        component={FormField}
        label={label}
        name={name}
        type={type} />
        <br/>
        </div>
      )
    })
  }
  
  render() {
    const { pristine, submitting } = this.props;

    return (
      <div className="container">
        <form onSubmit={this.props.handleSubmit}>
          {this.renderFields()}
          <button type="submit" className="btn btn-dark" disabled={pristine || submitting}>
            Adentro!
            
          </button>
        </form>
      </div>
    )
  }
}


function validate(values) {
  const errors = {};
  _.each(loginForm, ({ name }) => {
    if (!values[name]) {
      errors[name] = 'El valor no es valido';
    }
  });
  return errors;
}
const styles = {
  marginTop: '200px'
}
export default reduxForm({
  validate,
  form: 'loginForm',
  destroyOnUnmount: true
})(Login)