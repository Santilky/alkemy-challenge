import React, { Component } from 'react'
import { connect } from 'react-redux';
import * as actions from '../actions/';
import NewAppForm from './appsubcomponents/newappform'
import AppPreview from './appsubcomponents/app'
class NewApp extends Component {
  componentDidMount() {
    const{ fetchCategorias} = this.props
    fetchCategorias('')
  }
  submitApp = (values) => {
    console.log('--------------')
    console.log(values)
    const {crearApp, auth, history} = this.props
    crearApp(values, auth.token, history)
  }
  render() {
    const {categorias, newapp} = this.props
    let form = null
    if (newapp.newAppForm) {

      form = newapp.newAppForm.registeredFields
      console.log(form)
      let app = {
        nombre: form.nombre.name,
      }
    }
    return <div className="container">
          <div className="row">
            <div className="sm-col-4">
              <NewAppForm onSubmit={this.submitApp} categorias={categorias} />
            </div>
          </div>
        <div  className="sm-col-4">
          {/* {form == null ? <div/> : } */}
        </div>
      </div>
  }
}

const mapStateToProps = (state) => {
  return {auth: state.auth, categorias: state.categorias, newapp: state.newAppForm, preview: null}
}
export default connect(mapStateToProps, actions)(NewApp)