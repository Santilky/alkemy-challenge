const router = require('express').Router()
const db = require('../models').db
const hash = require('../util/hash').verify
const jwt = require('../util/jwt').generateJWT
const ValidateLogin = require('../schema/login')
const errors = require('../errors/errors')
router.post('/login', async (req, res) => {
  const credentials = req.body
  console.log('pego!')
  try {
    await ValidateLogin(credentials)
    const usuario = await db.usuarios.findOne({
      where: {
        username: credentials.username
      }
    })
    if (usuario == null) {
      throw new errors.InvalidCredentials('usuario incorrecto, sacar!!')
    }
    if (await hash(credentials.password, usuario.password)) {
      let payload
      if (usuario.tipodeusuario == 'cliente') {
        const cliente = await db.clientes.findOne({
          where: {
            usuarioId: usuario.id
          }
        })
        payload = {
          userId: cliente.id,
          accountType: usuario.tipodeusuario
        }
      } else {
        const desarrollador = await db.desarrolladores.findOne({
          where: {
            usuarioId: usuario.id
          }
        })
        payload = {
          userId: desarrollador.id,
          accountType: usuario.tipodeusuario
        }
      }
      const token = jwt(payload)
      res.status(201).json({token, tipodeusuario: usuario.tipodeusuario})
    } else {
      throw new errors.InvalidCredentials('password invalida')
    }
  } catch(error) {
    res.status(error.statusCode || 500).json({message: error.message || 'Internal error'})
  }
  
})

module.exports = router