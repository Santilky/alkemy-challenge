const router = require('express').Router()
const validateSchema = require('../schema/register')
const db = require('../models/').db
const _ = require('lodash')
const hash = require('../util/hash')
const errors = require('../errors/errors')
router.post('/', async (req, res) => {
  // console.log(req.params)
  // console.log(req.query)
  // console.log(req.body)
  console.log('La db es:')
  console.log(db)
  const credentials = req.body
  try {
    await validateSchema(credentials)
    const passwordHashed = await hash.hash(credentials.password)
    const validatedCredentials = {
      username: credentials.username,
      password: passwordHashed,
      tipodeusuario: credentials.tipodeusuario
    }
    const yaexiste = await db.usuarios.findOne({
      where: {
        username: validatedCredentials.username
      }
    })
    if (yaexiste != null) {
      throw new errors.AlreadyExists()
    }
    const newUser = await db.usuarios.create(validatedCredentials)
    let resultado
    if (validatedCredentials.tipodeusuario == 'cliente') {
      resultado = await db.clientes.create({
        usuarioId: newUser.id
      })
    } else {
      // No hay otro tipo, asumo que es desarrollador, ya está validado
      resultado = await db.desarrolladores.create({
        usuarioId: newUser.id
      })
    }
    res.status(201).json(resultado)
  } catch(error) {
    res.status(error.statusCode || 500).json({message: error.message})
  }
  
  
  //const passwordHashed = await hash.hash(credentials.password)
  
})

module.exports = router