const router = require('express').Router()
const validateApp = require('../schema/app')
const requireLogin = require('../middleware/requirelogin')
const requireDev = require('../middleware/requiredev')
const requireCliente = require('../middleware/requireclient')
const db = require('../models').db
const errors = require('../errors/errors')
const _ = require('lodash')
const app = require('../models/app')
const validarCambios = require('../schema/appcambios')

router.post('/', requireLogin, requireDev, async (req, res) => {
  const app = req.body
  const user = req.user.userId
  try {
    await validateApp(app)
    const appyaexistente = await db.apps.findOne({
      where: {
        nombre: app.nombre
      }
    })
    if (appyaexistente != null) {
      throw new errors.AlreadyExists('El nombre esta en uso')
    }
    app.desarrolladoreId = user
    const newApp = await db.apps.create(app)
    res.json(newApp)
  } catch(error) {
    res.status(error.statusCode || 500).json({message: error.message || 'Internal error'})
  }
})

router.get('/', requireLogin, async (req, res) => {
  const userid = req.user.userId
  const tipodeUsuario = req.user.accountType
  const query = req.query
  console.log(query)
  // categoria si no trae todo
  // pagina sino es 0
  // Agregar Hash con Redis
  let appsBuscadas = []
  let nopaginar = false
  try {
    if (tipodeUsuario == 'desarrollador') {
      appsBuscadas = await db.apps.findAll({
        where: {
          desarrolladoreId: userid
        }})
    } else {
      // Asumo que no hay otro rol restante ademas de cliente
      const categoria = query.categoria
      if (categoria != null || categoria != undefined) {
        appsBuscadas = await db.apps.findAll({
          where: {
            categoriaId: categoria
          }
        })
      } else if (query.tipo == 'misapps'){
        const misapps = await db.appscompradas.findAll({
          where: {
            clienteId: userid
          }
        })
        for (const miapp of misapps) {
          const app = await db.apps.findOne({
            where: {
              id: miapp.appId 
            }

          })
          if (app != null) {
            appsBuscadas.push(app)
          }
        }
        nopaginar = true
      } else if (query.tipo == 'misappsdeseadas') {
        const misapps = await db.productosdeseados.findAll({
          where: {
            clienteId: userid
          }
        })
        for (const miapp of misapps) {
          const app = await db.apps.findOne({
            where: {
              id: miapp.appId
            }
          })
          if (app != null) {
            appsBuscadas.push(app)
          }
        }
        nopaginar = true
      } else{
        appsBuscadas = await db.apps.findAll()
      }
    }
    const totaldeapps = appsBuscadas.length
    let resultados
    if (nopaginar) {
      resultados = appsBuscadas
    } else {
      const pagina = validarEntero(query.pagina)
      resultados = devolverPagina(appsBuscadas, pagina )
    }
    res.json({
      totaldeapps,
      resultados
    })
  } catch(error) {
    console.log(error)
    res.status(error.statusCode || 500).json({message: error.message || 'Internal error'})
  }
})

router.get('/guest', async (req, res) => {
  const query = req.query
  // categoria si no trae todo
  // pagina sino es 0
  // Agregar Hash con Redis
  let appsBuscadas
  try {
      const categoria = query.categoria
      if (categoria != null || categoria != undefined) {
        appsBuscadas = await db.apps.findAll({
          where: {
            categoriaId: categoria
          }
        })
      } else{
        appsBuscadas = await db.apps.findAll()
      }    
      
    const pagina = validarEntero(query.pagina)
    let resultados = devolverPagina(appsBuscadas, pagina ) 
    
    const totaldeapps = appsBuscadas.length
    
    res.json({
      totaldeapps,
      resultados
    })
  } catch(error) {
    console.log(error)
    res.status(error.statusCode || 500).json({message: error.message || 'Internal error'})
  }
})


function validarEntero(entero) {
  console.log(entero)
  if (_.isNumber(entero) && !_.isNaN(entero) && !_.isUndefined(entero) && entero != null && entero > 0) {
    return parseInt(entero)
  } else {
    return 1
  }
}
function devolverPagina(lista, numerodepagina) {
  const PAGESIZE = 10
  let pagina
  console.log(lista.length)
  if (lista.length == 0) {
    pagina = []
  } else {
    if (lista.length > (numerodepagina * PAGESIZE) - 10) {
      pagina = lista.splice(numerodepagina -1, 10)
    } else { 
      throw new errors.NotFound('Pagina invalida')
    }
  }
  return pagina
}

router.put('/', requireLogin, requireDev, async (req, res) => {
  try {
    const devid = req.user.userId
    const appid = req.body.id
    let cambios = req.body.cambios
    await validarCambios(cambios)
    const appbuscada = await db.apps.findOne({
      where: {
        desarrolladoreId: devid,
        id: appid
      }
    })
    if (appbuscada == null) {
      throw new errors.NotFound()
    }
    for (propiedad in cambios) {
      appbuscada[propiedad] = cambios[propiedad]
    }
    await appbuscada.save()
    res.status(202).json(appbuscada)
  } catch(error) {
    res.status(error.statusCode || 500).json({message: error.message || 'Internal error'})
  }
  
})

router.delete('/', requireLogin, requireDev, async (req, res) => {
  const appid = req.body.id
  const userid = req.user.userId
  try {
    const appbuscada = await db.apps.findOne({
      where: {
        desarrolladoreId: userid,
        id: appid
      }
    })
    if (appbuscada == null) {
      throw new errors.NotFound()
    }
    await appbuscada.destroy()
    res.status(203).json({message: `${appbuscada.nombre} ha sido eliminada`})
  } catch(error) {

  }
})

router.get('/deseo', requireLogin, requireCliente, async (req, res) => {
  const pagina = validarEntero(req.query.pagina)
  const userid = req.user.userId
  const listadeseados = await db.productosdeseados.findAll({
    where: {
      clienteId: userid
    }
  })
    const totaldeapps = listadeseados.length
    const resultados = devolverPagina(listadeseados, pagina )
    res.json({
      totaldeapps,
      resultados
    })
})
router.post('/deseo', requireLogin, requireCliente, async (req, res) => {
  try {
    const appid = req.body.appid
    const userid = req.user.userId
    const query = {
      clienteId: userid,
      appId: appid
    }
    const appyacomprada = await db.appscompradas.findOne({
      where: query
    })
    if (appyacomprada != null) {
      throw new errors.AlreadyExists('Ya has comprado la aplicacion')
    }
    const appyadeseada = await db.productosdeseados.findOne({
      where: query
    })
    if (appyadeseada != null) {
      throw new errors.AlreadyExists('Ya deseas esta aplicacion')
    }
    const newApp = await db.productosdeseados.create({
      clienteId: userid,
      appId: appid
    })
    res.status(201).json(newApp)
  } catch(error) {
    res.status(error.statusCode || 500).json({message: error.message || 'Internal error'})
  }
  
})
router.delete('/deseo/:id', requireLogin, requireCliente, async (req, res) => {
  try {
    const userid = req.user.userId
    const appid = req.params.id
    console.log(appid)
    const appdeseada = await db.productosdeseados.findOne({
      where:{
        clienteId: userid,
        appId: appid
      }
    })
    if (appdeseada == null) {
      throw new errors.NotFound('No se desea esta app')
    }
    console.log(appdeseada)
    await appdeseada.destroy()
    res.status(203).json({message: 'Se ha borrorado con exito'})
  } catch(error) {
    res.status(error.statusCode || 500).json({message: error.message || 'Internal error'})
  }
  
})

router.post('/compras', requireLogin, requireCliente, async (req, res) => {
  try {
    const appid = req.body.appid
    const userid = req.user.userId
    const query = {
      clienteId: userid,
      appId: appid
    }
    const appyacomprada = await db.appscompradas.findOne({
      where: query
    })
    if (appyacomprada != null) {
      throw new errors.AlreadyExists('Ya has comprado la aplicacion')
    }
    const newApp = await db.appscompradas.create({
      clienteId: userid,
      appId: appid
    })
    const appyadeseada = await db.productosdeseados.findOne({
      where: query
    })
    if (appyadeseada != null) {
      await appyadeseada.destroy()
    }
    res.status(201).json(newApp)
  } catch(error) {
    res.status(error.statusCode || 500).json({message: error.message || 'Internal error'})
  }
})

router.get('/compras', requireLogin, requireCliente, async (req, res) => {
  const userid = req.user.userId
  const query = req.body
  const categoria = req.body.categoria
  // categoria si no trae todo
  // pagina sino es 0
  // Agregar Cashe con Redis
  let appsBuscadas
  try {
    if (categoria != null || categoria != undefined) {
      appsBuscadas = await db.appscompradas.findAll({
        where: {
          clienteId: userid,
          categoriaId: categoria
        }
      })
    } else {
      appsBuscadas = await db.appscompradas.findAll({
        where: {
          clienteId: userid
        }
      })
    }
    const pagina = validarEntero(query.pagina)
    const totaldeapps = appsBuscadas.length
    const resultados = devolverPagina(appsBuscadas, pagina )
    
    res.json({
      totaldeapps,
      resultados
    })
  } catch(error) {
    res.status(error.statusCode || 500).json({message: error.message || 'Internal error'})
  }
})

module.exports = router