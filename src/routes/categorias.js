const router = require('express').Router()
const requireLogin = require('../middleware/requirelogin')
const requireDev = require('../middleware/requiredev')
const db = require('../models').db


router.get('/', async (req, res) => {
  const result = await db.categorias.findAll({})
  res.json(result)
})

module.exports = router