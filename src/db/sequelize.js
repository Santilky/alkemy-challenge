const { Sequelize} = require('sequelize')
const relations = require('../models').crearDB
const sequelize = new Sequelize({
  dialect: 'sqlite',
  storage: './database.sqlite',
  logging: false
})
let db = {}
const startServer = async () => {
  try {
    await sequelize.authenticate();
    db = relations(sequelize)
    await sequelize.sync({force: false})
    console.log('Se ha conectado a la base de datos.');
  } catch (error) {
    console.error('No se ha podido conectar a la base de datos', error);
  }
}

module.exports = {startServer, db}