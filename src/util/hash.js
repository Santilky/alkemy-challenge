const { Crypt, Compare } = require ('password-crypt');

const SECRET = process.env.PASSWORD_CRYPT_SECRET || 'Probando Hash123'

const hash = async (pwd) => Crypt(SECRET, pwd)
const verify = async (pwd, hashedPwd) => Compare(SECRET, pwd, hashedPwd)

module.exports = {hash, verify}