const jwt = require('jsonwebtoken')

const SECRET =  'PROBANDO 1234567'


const generateJWT = (payload) => {
  return jwt.sign(payload, SECRET,{ algorithm: 'HS256', expiresIn: '1h' })
} 

const verifyJWT = (token) => {
  return decoded = jwt.verify(token, SECRET)
}

module.exports = {generateJWT, verifyJWT}