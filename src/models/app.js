module.exports = (sequelize, Sequelize) => {
  const apps = sequelize.define('apps', {
    nombre: Sequelize.STRING,
    imageurl: Sequelize.STRING,
    precio: Sequelize.INTEGER
  })
  return apps
}