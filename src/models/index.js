const Sequelize = require('sequelize')
const apps = require('./app')
const categorias = require('./categoria')
const appscompradas = require('./appcomprada')
const clientes = require('./cliente')
const desarrolladores = require('./desarrollador')
const usuarios = require('./user')
const deseados = require('./deseados')
const productosdeseados = require('./productodeseado')
const db = {}
const crearDB = (sequelize) => {
  
  db.usuarios = usuarios(sequelize, Sequelize)
  db.clientes = clientes(sequelize, Sequelize)
  db.desarrolladores = desarrolladores(sequelize, Sequelize)
  db.clientes.belongsTo(db.usuarios)
  db.desarrolladores.belongsTo(db.usuarios)
  db.apps = apps(sequelize, Sequelize)
  db.apps.belongsTo(db.desarrolladores)
  db.categorias = categorias(sequelize, Sequelize)
  db.apps.belongsTo(db.categorias)
  db.appscompradas = appscompradas(sequelize, Sequelize)
  db.appscompradas.belongsTo(db.apps)
  db.appscompradas.belongsTo(db.clientes)
  db.deseados = deseados(sequelize, Sequelize)
  db.productosdeseados = productosdeseados(sequelize, Sequelize)
  db.deseados.belongsTo(db.clientes)
  db.productosdeseados.belongsTo(db.apps)
  db.clientes.hasMany(db.productosdeseados)
}
module.exports = {crearDB, db}

