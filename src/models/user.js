
module.exports = (sequelize, Sequelize) => {
  const usuarios = sequelize.define('usuarios', {
    username: Sequelize.STRING,
    password: Sequelize.STRING,
    tipodeusuario: Sequelize.STRING
  })
  return usuarios
}