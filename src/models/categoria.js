module.exports = (sequelize, Sequelize) => {
  const categorias = sequelize.define('categorias', {
    nombre: Sequelize.STRING,
  })
  return categorias
}