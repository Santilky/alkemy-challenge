const _ = require('lodash')
const Joi = require('joi');
const errors = require('../errors/errors')
module.exports = async (credentials) => {
  if (_.isEmpty(credentials)) {
    throw new errors.BadRequest('No se han ingresado datos')
  }
  try {
    const value = await schema.validateAsync(credentials)
    console.log(value)
  } catch(error) {
    console.log(error.message)
    throw new errors.BadRequest('Informacion incorrecta')
  }
}

const schema = Joi.object({
  nombre: Joi.string()
  .alphanum()
  .min(6)
  .max(20)
  .required(),
  imageurl: Joi.string().required(),
  precio: Joi.number().required(),
  categoriaId: Joi.number().required()
  
})