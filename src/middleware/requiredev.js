module.exports = (req, res, next) => {
  if (req.user.accountType == 'desarrollador') {
    next()
  } else {
    res.status(407).json({message: 'No tienes acceso a este recurso'})
  }
}