const jwt = require('../util/jwt').verifyJWT
const _ = require('lodash')

async function requireLogin(req, res, next) {
  const header = req.headers
  const token = header.authorization
  console.log(header)
  try {
    if (_.isUndefined(token)) {
      throw {statusCode: 406, message: 'Necesitas estar logeado'}
    }
    const payload = jwt(token.split(' ')[1])
    console.log(payload)
    const user = {
      userId: payload.userId,
      accountType: payload.accountType
    }
    req.user = user
    next()
  } catch (error) {
    res.status(error.statusCode || 406).json({message:  error.message || 'No tenes permisos para acceder a este recurso'})
  }
}

module.exports = requireLogin