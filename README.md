# Ejecutar Back end: npm start
# Ejecutar front end en client/challengeclient/: npm start

### la dB use SQLite para hacerlo facil y mandarselos ya hechos con algo de contenido(se puede modificar a casi cualquier db de ser necesario), este fue generado por el test que hay ahi hecho, que lo hice solo para probarlo. Podria haber usado Jest para hacer testing, pero no me dio el tiempo.



### Cosas que no llegue a tiempo:
Cambiar la pagina de apps si hubiesen en el front, actualmente solo va a devolver siempre la pagina 1, sin embargo el back end soporta paginacion, solo hay que especificarlo en el pedido
Usar Redis para cachear los requests por usuario y tipo
Validar bien los formularios y valores en el front end, el back end los valida dentro de todo con el uso de Joi y _
Manejar los errores en el front end, no llegue a manejar los errores, lo cual permitiria 
Darle mas estilo al front para que se vea atractivo y homogeneo

## Consideraciones que podrian estar mejor: 
Refactorizar codigo, tanto en el front como en el back, me quedó codigo repetido, agrupar mejor y nombrar mejor los componenentes del front y diversos archivos del backend, establecer un lenguaje, no mezclar ingles con español a lo bestia.
Mejorar los manejos de state, redux o context, actualmente lo hice medio a lo bestia, de pedir cada recurso o query nuevamente, manejando las respuestas podria haber hecho mas eficiente la app

## Muchas gracias por dejarme participar, manejo React Native para apps nativas, no manejaba React JS y me fue bastante desafiante, pense que iba a hacer el front en 8 horas y me llevó mas de 12, el back end alrededor de 6 horas.
