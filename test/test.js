const Axios = require('axios')
const baseURL = 'http://localhost:3500/api'
const axios = Axios.create({
  baseURL: 'http://localhost:3500/api'
})

const tests = []

async function newUser(username, password, tipodeusuario) {
  const user = await axios.post('/user', {
    username,
    password,
    repeat_password: password,
    tipodeusuario
  })
  return user
}

async function login(username, password) {
  const session = await axios.post('/session/login', {
    username,
    password
  })
  return session.data
}

async function createApp(app, token) {
   const headers = {
      'Content-Type': 'application/json',
      "Authorization": token
    }
  const appcreada = await Axios.post('http://localhost:3500/api/apps', app, {headers})
  return appcreada.data
}

async function editarApp(token, props) {
  const headers = {
    'Content-Type': 'application/json',
    "Authorization": token
  }
const appcreada = await Axios.put('http://localhost:3500/api/apps', props, {headers})
return appcreada.data
}
async function createClientAndLogin(username, password) {
  await newUser(username, password, 'cliente')
  const session = await login(username, password)
  return 'bearer ' + session.token
}
async function createDevAndLogin(username, password) {
  await newUser(username, password, 'desarrollador')
  const session = await login(username, password)
  return 'bearer ' + session.token
}

async function desearProducto(id, token) {
  const headers = {
    'Content-Type': 'application/json',
    "Authorization": token
  }
  const desearApp = await Axios.post('http://localhost:3500/api/apps/deseo', {appid: id}, {headers})
  return desearApp
}
async function borrarDesearProducto(id, token) {
  const headers = {
    'Content-Type': 'application/json',
    "Authorization": token
  }
  const desearApp = await Axios.delete('http://localhost:3500/api/apps/deseo/' + id, {headers})
  return desearApp
}
async function getDeseos(token) {
  const headers = {
    'Content-Type': 'application/json',
    "Authorization": token
  }
  const desearApp = await Axios.get('http://localhost:3500/api/apps/deseo', {headers})
  return desearApp.data
}

async function comprarApp(id, token) {
  const headers = {
    'Content-Type': 'application/json',
    "Authorization": token
  }
  const desearApp = await Axios.post('http://localhost:3500/api/apps/compras', {appid: id}, {headers})
  return desearApp.data
}

async function getAppsCompradas(token) {
  const headers = {
    'Content-Type': 'application/json',
    "Authorization": token
  }
  const desearApp = await Axios.get('http://localhost:3500/api/apps/compras', {headers})
  return desearApp.data
}
function crearAppJSON(nombre, precio, imageurl, categoriaId) {
  return {
    nombre,
    precio,
    categoriaId,
    imageurl
  }
}

async function test() {
  try {
    const apps = []
    const user1token = await createClientAndLogin('pepito1', 'mujica')
    const user2token = await createClientAndLogin('pepoto1', 'mujica')
    const user3token = await createClientAndLogin('pepote1', 'mujica')
    const user4token = await createDevAndLogin('hestiv', 'jobs123')
    const user5token = await createDevAndLogin('hestivjoba', 'jobs123')
    const user6token = await createDevAndLogin('marchelo', 'jobs123')

    apps[0] = crearAppJSON("FeisBook", 5, 'https://hatrabbits.com/wp-content/uploads/2017/01/random.jpg',2)
    apps[1] = crearAppJSON("FeisBook2", 0, 'https://hatrabbits.com/wp-content/uploads/2017/01/tafel-1.jpg',1)
    apps[2] = crearAppJSON("FeisBook3", 2, 'https://hatrabbits.com/wp-content/uploads/2017/01/random-word-1.jpg',3)
    apps[3] = crearAppJSON("FeisBook4", 3, 'http://asdknjsjn.com',1)
    apps[4] = crearAppJSON("FeisBook5", 2, 'https://hatrabbits.com/wp-content/uploads/2020/06/lampflower.jpg',2)
    const appcreada = []
    appcreada[0] = await createApp(apps[0], user4token)
    appcreada[1] = await createApp(apps[1], user5token)
    appcreada[2] = await createApp(apps[2], user6token)
    appcreada[3] = await createApp(apps[3], user5token)
    appcreada[4] = await createApp(apps[4], user4token)
    appcreada[2] = await editarApp(user6token, {
      id: appcreada[2].id,
      cambios: {
        precio: 30
      }
    })
    appcreada[3] = await editarApp(user5token, {
      id: appcreada[3].id,
      cambios: {
        imageurl: 'https://hatrabbits.com/wp-content/uploads/2016/03/oma.jpg'
      }
    })
    await desearProducto(appcreada[1].id, user2token)
    await desearProducto(appcreada[2].id, user2token)
    await desearProducto(appcreada[3].id, user2token)
    console.log(user2token)
    await borrarDesearProducto(appcreada[1].id, user2token)
    
    // console.log(appcreada)
    await comprarApp(appcreada[2].id, user2token)
    const deseos = await getDeseos(user2token)
    const compras = await getAppsCompradas(user2token)
    
    console.log(deseos)
    console.log(compras)
  } catch (error) {
    console.log(error)
  }
  
}
test()