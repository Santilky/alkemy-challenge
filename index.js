const Express = require('express')
const cors = require('cors')
const {startServer} = require('./src/db/sequelize')
const db = require('./src/models').db
const user = require('./src/routes/user')
const sesion = require('./src/routes/session')
const categorias = require('./src/routes/categorias')
const apps = require('./src/routes/apps')
const { use } = require('./src/routes/user')
const app = Express()
app.use(Express.json())
app.use(cors())
const port = 3500
app.use('/api/user/', user)
app.use('/api/session/', sesion)
app.use('/api/categorias/', categorias)
app.use('/api/apps/', apps)
startApp()

async function startApp() {
  await startServer()
  app.listen(port, async () => {
    // Esto no deberia estar aca... pero bueno xD
    // await db.categorias.create({
    //   nombre: 'Juegos'
    // })
    // await db.categorias.create({
    //   nombre: 'Apps'
    // })
    // await db.categorias.create({
    //   nombre: 'Social'
    // })
    console.log('Server Up! Port: ' + port)

  })
}